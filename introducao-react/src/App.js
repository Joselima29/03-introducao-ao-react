import './App.css';
import Welcome from './Welcome';


const text = "Hello World"

function App() {
  return (
    <div>
      <Welcome name={text}/>
      <Welcome name={text.toUpperCase()}/>
      <Welcome name={text.toLowerCase()}/>
      <Welcome name={text.split(' ').reverse()}/>
      <Welcome name={text.split('').join('-')}/>
      <Welcome name={text.concat(', ACCT!')}/>
      <Welcome name={text.concat(' : Tamanho = ',text.length)}/>
      <Welcome name={text.slice(0,6)}/>
      <Welcome name={text.replace("World", "Planet")}/>
      <Welcome name={text.split('').reverse()}/>
    </div>
  );
}

export default App;
